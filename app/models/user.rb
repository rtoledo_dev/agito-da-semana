# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  default_scope order(:name)
  scope :photographers, where(:photographer => true)
  scope :administrators, where(:admin => true)

  attr_accessible :about,:avatar, :email, :facebook_url, :name, :password, :twitter_url, :password_confirmation, :admin, :photographer
  
  has_one :columnist
  has_many :user_albums
  has_many :album_images, :through => :user_albums, :source => :user_album_images
  
  has_attached_file :avatar
  
  validates_presence_of :name, :email
  validates_presence_of :password, :on => :create
  validates_confirmation_of :password, :on => :create
  
  def to_param
    [self.id,self.name.parameterize].join('-')
  end
end
