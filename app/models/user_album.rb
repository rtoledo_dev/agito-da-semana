# -*- encoding : utf-8 -*-
class UserAlbum < ActiveRecord::Base
  belongs_to :user
  belongs_to :place
  belongs_to :event
  belongs_to :user_album_image
  attr_accessible :address, :description, :latitude, :longitude, :title
  validates_presence_of :title, :user_id
  validates_uniqueness_of :title, :scope => [:user_id,:title]
  has_many :user_album_images
end
