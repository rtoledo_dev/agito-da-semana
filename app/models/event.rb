# -*- encoding : utf-8 -*-
class Event < ActiveRecord::Base
  # default_scope order("realized_at DESC")
  belongs_to :event_category
  belongs_to :place
  has_many :galleries
  attr_accessible :address, :description, :is_special, :latitude, :longitude, :photo, :realized_at, :show_in_front, :title, :event_category_id, :place_id
  validates_presence_of :title, :realized_at, :event_category_id
  has_attached_file :photo, :styles => {:home => '350x390', :medium => "350", :big => "400"}
end
