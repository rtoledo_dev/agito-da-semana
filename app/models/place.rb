# -*- encoding : utf-8 -*-
class Place < ActiveRecord::Base
  default_scope order(:name)
  attr_accessible :address, :description, :latitude, :longitude, :name
  validates_presence_of :name, :address
  validates_uniqueness_of :name
end
