# -*- encoding : utf-8 -*-
class PollAnswer < ActiveRecord::Base
  belongs_to :poll
  attr_accessible :description
end
