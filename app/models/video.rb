# -*- encoding : utf-8 -*-
class Video < ActiveRecord::Base
  default_scope order('created_at DESC')
  belongs_to :video_category
  attr_accessible :title, :url, :video_category_id
  validates_presence_of :title, :url, :video_category_id
end
