# -*- encoding : utf-8 -*-
class UserAlbumImage < ActiveRecord::Base
  belongs_to :user_album
  attr_accessible :image
  has_attached_file :image, :styles => {:list => "370x152#", :detail => "500", :thumb => '75x75#', :big => "400"}
  
  after_save :set_front_image
  
  def set_front_image
    return unless self.user_album.user_album_image_id.nil?
    self.user_album.update_attribute(:user_album_image_id, self.id)
  end
  
  def name
    self.image_file_name
  end
end
