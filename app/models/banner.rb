# -*- encoding : utf-8 -*-
class Banner < ActiveRecord::Base
  module Types
    IMAGE, SWF = 0, 1
  end
  # default_scope order('created_at desc')
  scope :images, where(:type_file => Banner::Types::IMAGE)
  scope :top_full, joins(:banner_positions).where(:banner_positions => {:banner_place_id => 1}).order("rand()")
  scope :top_small, joins(:banner_positions).where(:banner_positions => {:banner_place_id => 2}).order("rand()")
  scope :middle_big, joins(:banner_positions).where(:banner_positions => {:banner_place_id => 3}).order("rand()")
  scope :left_small, joins(:banner_positions).where(:banner_positions => {:banner_place_id => 4}).order("rand()")
  scope :footer_full, joins(:banner_positions).where(:banner_positions => {:banner_place_id => 5}).order("rand()")
  scope :front, joins(:banner_positions).where(:banner_positions => {:banner_place_id => 6}).order("rand()")
  scope :home_middle, joins(:banner_positions).where(:banner_positions => {:banner_place_id => 7}).order("rand()") 
 
  attr_accessible :image, :swf, :title, :type_file, :url, :place_ids
  validates_presence_of :title, :type_file, :place_ids
  has_attached_file :image, :styles => {
    :top_full => '728x98',
    :top_small => '318x110',
    :middle_big => '750x300',
    :left_small => '230x250',
    :footer_full => '728x98',
    :front => '900',
    :home_middle => '350'
  }
  has_attached_file :swf
  has_many :banner_positions
  has_many :places, :through => :banner_positions, :source => :banner_place
  
  def for_image?
    self.type_file == Types::IMAGE
  end
end
