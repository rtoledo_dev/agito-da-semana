# -*- encoding : utf-8 -*-
class NewsRelation < ActiveRecord::Base
  belongs_to :news_category
  belongs_to :news
  # attr_accessible :title, :body
end
