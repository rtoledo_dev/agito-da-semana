# -*- encoding : utf-8 -*-
class Columnist < ActiveRecord::Base
  belongs_to :user
  attr_accessible :theme
  delegate :name, :to => :user
end
