# -*- encoding : utf-8 -*-
module AgendasHelper
  def next_event
    Event.where(:show_in_front => true).order("RAND()").first
  end
end
