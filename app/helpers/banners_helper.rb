# -*- encoding : utf-8 -*-
module BannersHelper
  def front_banner
    @front_banner ||= print_banner(Banner.front.first,:front)
  end
  
  def top_full_banner
    print_banner(Banner.top_full.first,:top_full)
  end
  
  def top_3_banners
    output = []
    Banner.top_small.limit(3).each_with_index do |banner,i|
      output << content_tag(:div, print_banner(banner,:top_small), :class => (i == 0 ? 'first' : nil))
    end
    output.join.html_safe
  end
  
  def left_6_banners
    output = []
    Banner.left_small.limit(8).each_with_index do |banner,i|
      output << content_tag(:div, print_banner(banner,:left_small), :class => 'left-banner')
    end
    output.join.html_safe
  end
  
  def middle_big_banners
    @middle_big_banners ||= Banner.middle_big.images.limit(10) #.first,:middle_big)
  end

  def home_middle_banner
    print_banner(Banner.home_middle.first,:home_middle)
  end
  
  def footer_full_banner
    print_banner(Banner.footer_full.first,:footer_full)
  end
  
  def print_banner(banner,size_definition, image_html = {})
    return "" if banner.nil?
    
    output = ""
    if banner.for_image?
      image = image_tag(banner.image.url(size_definition), image_html.merge(:size => banner.image.styles[size_definition].geometry))
      output = banner.url.blank? ? image : link_to(image,banner.url, :target => "_blank")
    else
      width, height = banner.image.styles[size_definition].geometry.split('x')
      output = content_tag(:div,'',:id => "banner_#{banner.id}_#{size_definition}")
      output = output + javascript_tag do
        "$('#banner_#{banner.id}_#{size_definition}').flash({
            src: '#{banner.swf.url}',
            width: #{width},
            height: #{height}
        });"
      end
    end
    output.html_safe
  end
end
