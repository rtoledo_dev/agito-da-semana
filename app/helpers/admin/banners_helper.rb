# -*- encoding : utf-8 -*-
module Admin::BannersHelper
  def type_of_banner(banner)
    banner.for_image? ? 'Imagem' : 'Arquivo Flash'
  end
end
