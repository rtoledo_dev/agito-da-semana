# -*- encoding : utf-8 -*-
module GaleriasHelper
  def last_6_galleries
    Gallery.common.limit(6)
  end
end
