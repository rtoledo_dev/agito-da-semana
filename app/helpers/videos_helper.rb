# -*- encoding : utf-8 -*-
module VideosHelper
  def list_video_categories_link
    links = VideoCategory.all.collect do |video_category|
      link_to(video_category.name, videos_path(:categoria => video_category.id))
    end
    links.join(', ').html_safe
  end
end
