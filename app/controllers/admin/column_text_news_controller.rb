# -*- encoding : utf-8 -*-
class Admin::ColumnTextNewsController < Admin::AdminController
  # GET /admin/column_text_news
  # GET /admin/column_text_news.json
  def index
    @column_text_news = ColumnTextNews.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @column_text_news }
    end
  end

  # GET /admin/column_text_news/1
  # GET /admin/column_text_news/1.json
  def show
    @column_text_news = ColumnTextNews.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @column_text_news }
    end
  end

  # GET /admin/column_text_news/new
  # GET /admin/column_text_news/new.json
  def new
    @column_text_news = ColumnTextNews.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @column_text_news }
    end
  end

  # GET /admin/column_text_news/1/edit
  def edit
    @column_text_news = ColumnTextNews.find(params[:id])
  end

  # POST /admin/column_text_news
  # POST /admin/column_text_news.json
  def create
    @column_text_news = ColumnTextNews.new(params[:column_text_news])

    respond_to do |format|
      if @column_text_news.save
        format.html { redirect_to column_text_news_index_path, :notice => 'Column text news was successfully created.' }
        format.json { render :json => @column_text_news, :status => :created, :location => @column_text_news }
      else
        format.html { render :action => "new" }
        format.json { render :json => @column_text_news.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/column_text_news/1
  # PUT /admin/column_text_news/1.json
  def update
    @column_text_news = ColumnTextNews.find(params[:id])

    respond_to do |format|
      if @column_text_news.update_attributes(params[:column_text_news])
        format.html { redirect_to column_text_news_index_path, :notice => 'Column text news was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @column_text_news.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/column_text_news/1
  # DELETE /admin/column_text_news/1.json
  def destroy
    @column_text_news = ColumnTextNews.find(params[:id])
    @column_text_news.destroy

    respond_to do |format|
      format.html { redirect_to admin_column_text_news_index_url }
      format.json { head :no_content }
    end
  end
end
