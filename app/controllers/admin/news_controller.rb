# encoding: utf-8
class Admin::NewsController < Admin::AdminController
  uses_tiny_mce(:options => AppConfig.default_mce_options, :only => [:new, :edit])
  
  
  # GET /admin/news
  # GET /admin/news.json
  def index
    @news = News.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @news }
    end
  end

  # GET /admin/news/1
  # GET /admin/news/1.json
  def show
    @news = News.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @news }
    end
  end

  # GET /admin/news/new
  # GET /admin/news/new.json
  def new
    @news = News.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @news }
    end
  end

  # GET /admin/news/1/edit
  def edit
    @news = News.find(params[:id])
  end

  # POST /admin/news
  # POST /admin/news.json
  def create
    @news = News.new(params[:news])

    respond_to do |format|
      if @news.save
        format.html { redirect_to admin_news_index_path, :notice => 'Notícia criada com sucesso!' }
        format.json { render :json => @news, :status => :created, :location => @news }
      else
        flash.now[:error] = 'Erro ao criar a notícia'
        format.html { render :action => "new" }
        format.json { render :json => @news.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/news/1
  # PUT /admin/news/1.json
  def update
    @news = News.find(params[:id])

    respond_to do |format|
      if @news.update_attributes(params[:news])
        format.html { redirect_to admin_news_index_path, :notice => 'Notíca atualizada com sucesso.' }
        format.json { head :no_content }
      else
        flash.now[:error] = 'Erro ao atualizar a notícia'
        format.html { render :action => "edit" }
        format.json { render :json => @news.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/news/1
  # DELETE /admin/news/1.json
  def destroy
    @news = News.find(params[:id])
    @news.destroy

    respond_to do |format|
      format.html { redirect_to admin_news_index_url }
      format.json { head :no_content }
    end
  end
end
