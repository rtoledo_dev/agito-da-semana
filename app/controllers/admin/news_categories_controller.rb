# -*- encoding : utf-8 -*-
class Admin::NewsCategoriesController < Admin::AdminController
  # GET /admin/news_categories
  # GET /admin/news_categories.xml
  def index
    @news_categories = NewsCategory.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @news_categories }
    end
  end

  # GET /admin/news_categories/1
  # GET /admin/news_categories/1.xml
  def show
    @news_category = NewsCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @news_category }
    end
  end

  # GET /admin/news_categories/new
  # GET /admin/news_categories/new.xml
  def new
    @news_category = NewsCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @news_category }
    end
  end

  # GET /admin/news_categories/1/edit
  def edit
    @news_category = NewsCategory.find(params[:id])
  end

  # POST /admin/news_categories
  # POST /admin/news_categories.xml
  def create
    @news_category = NewsCategory.new(params[:news_category])

    respond_to do |format|
      if @news_category.save
        format.html { redirect_to(admin_news_categories_url, :notice => 'News category was successfully created.') }
        format.xml  { render :xml => @news_category, :status => :created, :location => @news_category }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @news_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/news_categories/1
  # PUT /admin/news_categories/1.xml
  def update
    @news_category = NewsCategory.find(params[:id])

    respond_to do |format|
      if @news_category.update_attributes(params[:news_category])
        format.html { redirect_to(admin_news_categories_url, :notice => 'News category was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @news_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/news_categories/1
  # DELETE /admin/news_categories/1.xml
  def destroy
    @news_category = NewsCategory.find(params[:id])
    @news_category.destroy

    respond_to do |format|
      format.html { redirect_to(admin_news_categories_url) }
      format.xml  { head :ok }
    end
  end
end
