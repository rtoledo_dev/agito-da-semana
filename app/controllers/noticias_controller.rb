# -*- encoding : utf-8 -*-
class NoticiasController < ApplicationController
  def index
    @news = News.paginate(:page => params[:page], :per_page => 10)
  end
  
  def detalhes
    @news = News.find(params[:id])
  end
end
