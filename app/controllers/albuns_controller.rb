# -*- encoding : utf-8 -*-
class AlbunsController < ApplicationController
  
  before_filter :check_logged, :only => [:criar,:adicionar_fotos,:salvar_foto, :remover_foto]
  before_filter :tagging_paths_to_js
  
  def index
    @user = User.find(params[:user_id])
    unless @user
      redirect_to principal_path, :notice => 'Usuário inexistente'
    end
    @user_albums = @user.user_albums.paginate(:page => params[:page], :per_page => 1)
  end
  
  def criar
    @user_album = current_user.user_albums.build(params[:user_album])
    if @user_album.save
      flash[:notice] = 'Álbum criado com sucesso. Adicione fotos agora mesmo para você e seus amigos curtirem!'
    end
    respond_to do |format|
      format.js
    end
  end
  
  def detalhes
    @user_album = UserAlbum.find(params[:id])
  end

  def adicionar_fotos
    @user_album = current_user.user_albums.where(:id => params[:album_id]).first
    if @user_album.nil?
      flash[:error] = 'Este álbum não lhe pertence. Não tente novamente ou será banido do site.'
      redirect_to principal_path
    end
  end
  
  def salvar_foto
    @user_album = current_user.user_albums.where(:id => params[:album_id]).first
    if @user_album.nil?
      render :json => { :result => 'error'}, :content_type => 'text/html'
    end
    
    @user_album_image = @user_album.user_album_images.build(params[:user_album_image])
    if @user_album_image.save
      render :json => { 
        :remover_path => remover_foto_path(@user_album,@user_album_image),
        :id => @user_album_image.id,
        :normal_path => @user_album_image.image.url.to_s , 
        :thumb_path => @user_album_image.image.url(:thumb).to_s , 
        :name => @user_album_image.image_file_name }, :content_type => 'text/html'
    else
      #todo handle error
      render :json => { :result => 'error'}, :content_type => 'text/html'
    end
  end
  
  def remover_foto
    @errors     = false
    @user_album = current_user.user_albums.where(:id => params[:album_id]).first
    @errors     = true if @user_album.nil?

    @user_album_image = @user_album.user_album_images.find(params[:id])
    
    @user_album_image.destroy if @user_album_image
    
    respond_to do |f|
      f.js
    end
  end
  
  
  def tagging_paths_to_js
    gon.salvar_marcacao_path  = album_salvar_marcacao_path
    gon.remover_marcacao_path = album_remover_marcacao_path
    gon.marcacoes_path        = album_marcacoes_path
  end
  
  
end
