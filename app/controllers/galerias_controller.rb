# -*- encoding : utf-8 -*-
class GaleriasController < ApplicationController
  def index
    @galleries = Gallery.common.paginate(:page => params[:page], :per_page => 18)
  end
  
  def detalhes
    @gallery = Gallery.find(params[:id])
  end

  
end
