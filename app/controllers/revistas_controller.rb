# -*- encoding : utf-8 -*-
class RevistasController < ApplicationController
  def index
    @magazines = Magazine.paginate(:page => params[:page], :per_page => 10)
  end
  
  def detalhes
    @magazine = Magazine.find(params[:id])
  end

end
