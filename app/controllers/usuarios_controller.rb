# -*- encoding : utf-8 -*-
class UsuariosController < ApplicationController
  before_filter :redirect_if_logged, :only => [:index, :registrar, :criar]
  
  def index
  end
  
  def registrar
  end
  
  def entrar
    user = User.where(:email => params[:email], :password => params[:password]).first
    if user
      session[:current_user] = user.attributes
      redirect_to principal_path, :notice => "Seja bem vindo(a) #{user.name}"
    else
      flash.now[:error] = 'Erro com usuário/senha'
      render :index
    end
  end
  
  def  sair
    session[:current_user] = nil
    redirect_to principal_path, :notice => "Volte sempre!"
  end
  
  def criar
    @user              = User.new(params[:user])
    @user.admin        = false
    @user.photographer = false
    if @user.save
      session[:current_user] = @user.attributes
      redirect_to principal_path, :notice => 'Parabéns sua conta foi criada com sucesso!'
    else
      flash.now[:error] = 'Erro ao criar sua conta.'
      render :action => :registrar
    end
  end
  
  def autocomplete
    render :json => User.all.collect{|t| {:id => t.id, :label => t.name, :value => t.name}}
  end

  
end
