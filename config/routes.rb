# -*- encoding : utf-8 -*-
Agitosemana::Application.routes.draw do
  match "garota" => 'garota#index'

  get "busca/resultados"

  get "login/index"
  
  match "principal" => 'home#principal', :as => :principal

  match "albuns-de/:user_id" => 'albuns#index', :as => :albuns
  match "albuns/detalhes/:id" => 'albuns#detalhes', :as => :detalhes_album
  match "albuns/criar" => 'albuns#criar', :as => :criar_album, :via => :post
  match "albuns/:album_id/adicionar-fotos" => 'albuns#adicionar_fotos', :as => :adicionar_fotos
  match "albuns/:album_id/salvar-foto" => 'albuns#salvar_foto', :as => :salvar_foto, :via => :post
  
  match "albuns/:album_id/:id/remover-foto" => 'albuns#remover_foto', :as => :remover_foto
  

  match "albuns/:album_id/ver-foto/:id" => 'albuns_fotos#ver', :as => :album_ver_foto
  match 'albuns/salvar-marcacao' => 'albuns_fotos#salvar_marcacao', :as => :album_salvar_marcacao, :via => :get
  match 'albuns/remover-marcacao' => 'albuns_fotos#remover_marcacao', :as => :album_remover_marcacao, :via => :get
  match 'albuns/fotos/marcacoes' => 'albuns_fotos#marcacoes', :as => :album_marcacoes, :via => :get


  match "entrar" => 'usuarios#index', :as => :entrar
  
  match "registrar" => "usuarios#registrar", :as => :usuarios_registrar

  post "usuarios/entrar"
  
  post "usuarios/criar"
  
  get "usuarios/autocomplete"
  
  match "sair" => "usuarios#sair", :as => :usuarios_sair

  match "chame-agito" => "agito#chame_agito", :as => 'chame_agito'
  match "chame-agito/enviar" => "agito#enviar_chame_agito", :as => 'enviar_chame_agito'
  

  match "fale-conosco" => "agito#fale_conosco", :as => 'fale_conosco'
  match "fale-conosco/enviar" => "agito#enviar_fale_conosco", :as => 'enviar_fale_conosco', :via => :post

  match "15-anos" => "agito#index_15_anos", :as => 'index_15_anos'
  match "15-anos/enviar" => "agito#enviar_15_anos", :as => 'enviar_15_anos'

  match "noticias" => "noticias#index", :as => 'noticias'
  match "noticias/detalhes/:id" => "noticias#detalhes", :as => 'detalhes_noticia'

  match "videos" => "videos#index", :as => 'videos'
  get "videos/detalhes"

  match "agendas" => "agendas#index", :as => 'agendas'
  match "agendas/detalhes/:id" => "agendas#detalhes", :as => 'detalhes_agenda'


  match "galerias" => "galerias#index", :as => :galerias
  match "galerias/detalhes/:id" => 'galerias#detalhes', :as => :detalhes_galeria
  match "galerias/:gallery_id/ver-foto/:id" => 'galerias_fotos#ver', :as => :galeria_ver_foto
  match "galerias/:gallery_id/comentar-foto/:id" => 'galerias_fotos#comentar', :as => :galeria_comentar_foto

  match "colirios" => "colirios#index", :as => :colirios
  match "colirios/detalhes/:id" => 'colirios#detalhes', :as => :detalhes_colirio
  
  
  match "revistas" => "revistas#index", :as => 'revistas'
  match "revistas/detalhes/:id" => "revistas#detalhes", :as => 'detalhes_revista'

  namespace :admin do
    root :to => "events#index"
    
    match 'login/index'               => 'login#index',   :as => :login
    match 'login/logout'              => 'login#logout',  :as => :logout
    match 'login/enter'               => 'login#enter',   :as => :login_enter, :via => :post
    
    
    resources :events
    resources :event_categories
    
    resources :user_albuns
    resources :galleries do
      resources :gallery_images do
        member do
          get :make_special
        end
      end
    end
    resources :gallery_categories
    
    resources :videos
    resources :video_categories
    
    resources :places
    
    resources :polls
    
    resources :news
    resources :news_categories
    resources :column_text_news
    resources :columnists
    
    resources :banners
    resources :magazines
    
    resources :users
    
    
  end
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'home#index'
end
