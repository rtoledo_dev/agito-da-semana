# -*- encoding : utf-8 -*-
User.create!(:email => 'administrador@email.com.br',
:name => 'Administrador',
:password => 'raposa',
:admin => true)

1.upto(2) do |i|
  1.upto(3) do
    user = User.new(:email => Random.email,
    :name => "#{Random.firstname} #{Random.lastname} #{Random.lastname}",
    :photographer => ((i % 2) == 0),
    :password => 'raposa')
    user.build_columnist(:theme => "#{Random.firstname} #{Random.lastname} #{Random.lastname}")
    user.save!
  end
end

"Topo grande, Topo pequeno, Grande no meio da página, Lateral direita, Final grande".split(',').each do |name|
  BannerPlace.create!(:name => name.strip)
end

Dir.entries("#{Rails.root}/db/images/").each do |image|
  next if image == '.' || image == '..'
  banner = Banner.new(:title => "#{Random.firstname} #{Random.lastname} #{Random.lastname}",
  :type_file => Banner::Types::IMAGE,
  :place_ids => BannerPlace.all.collect{|t| t.id},
  :image => File.open("#{Rails.root}/db/images/#{image}")
  )
  banner.save!
end

"Festas, Eventos, Vídeo-clips".split(',').each do |name|
  VideoCategory.create!(:name => name.strip)
end

"Policial, Prefeitura de Caratinga, Emprego, Economia".split(',').each do |name|
  NewsCategory.create!(:name => name.strip)
end

"15 Anos, Boates, Carnaval, Outros, Shows, Caminhada".split(',').each do |name|
  EventCategory.create!(:name => name.strip)
  GalleryCategory.create!(:name => name.strip)
end

"Armazém Diesel, Bar da Joana, Bloco Paga Pau, Boate Diesel, Boate Monalisa, Clube America, Esporte Clube Caratinga, Festa Particular, Kartódromo - Ipatinga, Mansão - Inhapim, Parque de Exposições - Governador Valadares, Rua de Caratinga, Serranos Bar, Sitio Rezende Lima, UNEC 1, Casarão".split(',').each do |name|
  Place.create!(:name => name.strip, :address => "#{Random.address_line_1} #{Random.address_line_2} #{Random.zipcode}")
end

1.upto(20) do
  Event.create!(:title => "#{Random.firstname} #{Random.lastname} #{Random.lastname}",
  :event_category_id => EventCategory.first.id,
  :show_in_front => true,
  :is_special => true,
  :place_id => Place.first.id,
  :realized_at => Random.date,
  :description => Random.paragraphs(3),
  :photo => File.open("#{Rails.root}/db/images/turned-wall5-1280x800.jpg")
  )
  
  news = News.create!(:title => "#{Random.firstname} #{Random.lastname} #{Random.lastname}",
  :short_description => Random.paragraphs,
  :show_in_front => true,
  :is_special => true,
  :content => Random.paragraphs(5),
  :photo => File.open("#{Rails.root}/db/images/brisbane_city_1680x1050_by_mrchuang.jpg")
  )
  news.categories << NewsCategory.all
  news.save!
  
end

1.upto(10) do
  Gallery.create!(:gallery_category_id => GalleryCategory.first.id,
  :type_gallery => Gallery::Types::COMMON,
  :event_id => Event.first.id,
  :place_id => Place.first.id,
  :title => "#{Random.firstname} #{Random.lastname} #{Random.lastname}",
  :description => Random.paragraphs(3),
  :realized_at => Random.date
  )
  
  
  Gallery.create!(:gallery_category_id => GalleryCategory.last.id,
  :type_gallery => Gallery::Types::COLLYRIUM,
  :event_id => Event.first.id,
  :place_id => Place.first.id,
  :title => "#{Random.firstname} #{Random.lastname} #{Random.lastname}",
  :description => Random.paragraphs(3),
  :realized_at => Random.date
  )
end

Gallery.where(:gallery_image_id => nil).each do |gallery|
  gallery_image = gallery.gallery_images.build(:image => File.open("#{Rails.root}/db/images/wall5-1280x800.jpg"))
  gallery_image.save!
end


gallery   = Gallery.common.first
collyrium = Gallery.collyrium.first

1.upto(25) do
  gallery_gallery_image = gallery.gallery_images.build(:image => File.open("#{Rails.root}/db/images/wall5-1280x800.jpg"))
  gallery_gallery_image.save!
  
  collyrium_gallery_image = collyrium.gallery_images.build(:image => File.open("#{Rails.root}/db/images/wall5-1280x800.jpg"))
  collyrium_gallery_image.save!
end

Event.update_all(:show_in_front => true, :is_special => true)
News.update_all(:show_in_front => true, :is_special => true)
