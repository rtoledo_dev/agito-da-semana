# -*- encoding : utf-8 -*-
class CreatePollVotes < ActiveRecord::Migration
  def self.up
    create_table :poll_votes do |t|
      t.references :poll
      t.references :poll_answer
      t.string :ip

      t.timestamps
    end
  end

  def self.down
    drop_table :poll_votes
  end
end
