# -*- encoding : utf-8 -*-
class CreateNews < ActiveRecord::Migration
  def self.up
    create_table :news do |t|
      t.string :title
      t.has_attached_file :photo
      t.string :short_description
      t.text :content
      t.boolean :show_in_front
      t.boolean :is_special

      t.timestamps
    end
  end

  def self.down
    drop_table :news
  end
end
