# -*- encoding : utf-8 -*-
class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :email
      t.string :password
      t.string :name
      t.has_attached_file :avatar
      t.text :about
      t.string :facebook_url
      t.string :twitter_url
      t.boolean :admin
      t.boolean :photographer

      t.timestamps
    end
  end

  def self.down
    drop_table :users
  end
end
