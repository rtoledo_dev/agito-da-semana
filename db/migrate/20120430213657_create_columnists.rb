# -*- encoding : utf-8 -*-
class CreateColumnists < ActiveRecord::Migration
  def self.up
    create_table :columnists do |t|
      t.references :user
      t.string :theme

      t.timestamps
    end
  end

  def self.down
    drop_table :columnists
  end
end
