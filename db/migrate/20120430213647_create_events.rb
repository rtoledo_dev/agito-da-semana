# -*- encoding : utf-8 -*-
class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.references :event_category
      t.string :title
      t.has_attached_file :photo
      t.references :place
      t.string :address
      t.string :latitude
      t.string :longitude
      t.text :description
      t.datetime :realized_at
      t.boolean :show_in_front
      t.boolean :is_special

      t.timestamps
    end
  end

  def self.down
    drop_table :events
  end
end
