# -*- encoding : utf-8 -*-
class CreateVideos < ActiveRecord::Migration
  def self.up
    create_table :videos do |t|
      t.references :video_category
      t.string :title
      t.string :url

      t.timestamps
    end
  end

  def self.down
    drop_table :videos
  end
end
