# -*- encoding : utf-8 -*-
class CreateGalleryCategories < ActiveRecord::Migration
  def self.up
    create_table :gallery_categories do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :gallery_categories
  end
end
