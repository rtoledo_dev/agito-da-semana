# -*- encoding : utf-8 -*-
class CreateTwitterMessages < ActiveRecord::Migration
  def self.up
    create_table :twitter_messages do |t|
      t.string :t_id
      t.string :message
      t.string :from

      t.timestamps
    end
  end

  def self.down
    drop_table :twitter_messages
  end
end
